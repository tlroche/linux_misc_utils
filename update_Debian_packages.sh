#!/usr/bin/env bash

### Update Debian packages from tty (!tty7) with logging:
### takes log filepath on commandline, or uses own default filepath.
### Main usecase/value-add for this script is running from (e.g.) tty1 for extreme CYA for
### (e.g.) kernel and GUI package updating.

### USAGE: update_Debian_packages.sh [path to logfile]

### TODO:
### * make compliant with Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml
### * define helper dir/folder, instead of requiring all helpers to be symlinked into *this* dir/folder.
### * move helper consts to properties file
### * if helpers not found, offer to install (and enable that offer!)
### * show $USAGE from `-h|--help`
### * see ./README.rst#TODOs

### Copyright (C) 2017-8 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### https://bitbucket.org/tlroche/linux_misc_utils/src/HEAD/update_Debian_packages.sh

###----------------------------------------------------------------------
### constants
###----------------------------------------------------------------------

## for logging

#THIS_FP="${0}"                      # following `readlink` failed on an old box, not sure why, ...
declare -r THIS_FP="${BASH_SOURCE}"  # ... but this fixed
logfile_fp_arg="${1}"
declare -r THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
declare -r THIS_FN="$(basename ${THIS_FP})"
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r DATE_FORMAT='%Y%m%d_%H%M'
declare -r TIMESTAMP="$(date +${DATE_FORMAT})"

# default path for logfile (if not given or writeable)
declare -r DEFAULT_LOGFILE_FN="${THIS_FN%%\.sh}_log__${TIMESTAMP}.txt"
if [[ ( -z "${THIS_DIR}" ) || ( "${THIS_DIR}" == '/' ) ]] ; then
    DEFAULT_LOGFILE_DIR='/tmp'
else
    DEFAULT_LOGFILE_DIR="${THIS_DIR}"
fi
declare -r DEFAULT_LOGFILE_FP="${DEFAULT_LOGFILE_DIR}/${DEFAULT_LOGFILE_FN}"
# echo -e "${DEBUG_PREFIX} DEFAULT_LOGFILE_FP='${DEFAULT_LOGFILE_FP}'"
logfile_fp="$(readlink -f ${logfile_fp_arg})" # FQ/absolute path
# echo -e "${DEBUG_PREFIX} logfile_fp='${logfile_fp}'"

## helpers

# replace with (e.g.) `apt-get` as desired
declare -r APT_HELPER='aptitude'
declare -r APT_UPDATE_HELPER="${APT_HELPER} update"
declare -r APT_UPGRADE_QUERY_HELPER="${APT_HELPER} -s full-upgrade"
declare -r APT_UPGRADE_HELPER="${APT_HELPER} -y full-upgrade"

declare -r STATE_QUERY_HELPER_FN='get_linux_versions.sh'
#STATE_QUERY_HELPER_FP="${THIS_DIR}/linux_version_getters/${STATE_QUERY_HELPER_FN}"
# Nope, gotta symlink to ${THIS_DIR} in order to run from (e.g.) ~/bin/
declare -r STATE_QUERY_HELPER_FP="${THIS_DIR}/${STATE_QUERY_HELPER_FN}"
declare -r STATE_QUERY_HELPER="${STATE_QUERY_HELPER_FP}"

###----------------------------------------------------------------------
### code
###----------------------------------------------------------------------

### ----------------------------------------------------------------------
### functions
### ----------------------------------------------------------------------

### TODO: move to standard-functions file
function setup_logging(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''

    if   [[ -z "${logfile_fp}" ]] ; then
        logfile_fp="${DEFAULT_LOGFILE_FP}"
        if   [[ -z "${logfile_fp}" ]] ; then
            echo "${FN_ERROR_PREFIX}: logfile_fp not defined, exiting..." 1>&2
            echo # newline
            exit 2
        fi
    fi

    if   [[ -r "${logfile_fp}" ]] ; then
        echo "${FN_ERROR_PREFIX}: logfile='${logfile_fp}' exists: move or delete. Exiting..." 1>&2
        echo # newline
        exit 3
    else
        touch "${logfile_fp}"
        ret_val="$?"
        if [[ ( "${ret_val}" -ne 0 ) || ( ! -w "${logfile_fp}" ) ]] ; then
            echo "${FN_ERROR_PREFIX}: cannot write logfile='${logfile_fp}', exiting..." 1>&2
            echo # newline
            exit 4
        else
            echo "${FN_MESSAGE_PREFIX}: logging to file='${logfile_fp}'" | tee -a "${logfile_fp}"
            return 0
        fi
    fi
    return 0
} # end function setup_logging

function test_helpers(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''

    ## FQed helpers
    if   [[ -z "${STATE_QUERY_HELPER}" ]] ; then
        echo "${FN_ERROR_PREFIX}: STATE_QUERY_HELPER not defined, exiting..." 1>&2 | tee -a "${logfile_fp}"
        echo | tee -a "${logfile_fp}" # newline
        exit 5
    elif [[ ! -x "${STATE_QUERY_HELPER}" ]] ; then
        echo "${FN_ERROR_PREFIX}: cannot run '${STATE_QUERY_HELPER}', exiting..." 1>&2 | tee -a "${logfile_fp}"
        echo | tee -a "${logfile_fp}" # newline
        exit 6
    fi

    ## `PATH`ed helpers
    if   [[ -z "${APT_HELPER}" ]] ; then
        echo "${FN_ERROR_PREFIX}: APT_HELPER not defined, exiting..." 1>&2 | tee -a "${logfile_fp}"
        echo | tee -a "${logfile_fp}" # newline
        exit 7
    # better way to test for item on PATH?
    elif [[ -z "$(which ${APT_HELPER})" ]] ; then
        echo "${FN_ERROR_PREFIX}: cannot find '${APT_HELPER}' on \$PATH, exiting..." 1>&2 | tee -a "${logfile_fp}"
        echo | tee -a "${logfile_fp}" # newline
        exit 8
    fi
    return 0
} # end function test_helpers

function payload(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    local ret_val=''
    local cmd=''

    for cmd in \
        "${STATE_QUERY_HELPER}"                       \
        'date'                                        \
        "sudo ${APT_UPDATE_HELPER}"                   \
        'date'                                        \
        "echo 'y' | sudo ${APT_UPGRADE_QUERY_HELPER}" \
        "ls -al ${logfile_fp}"                        \
    ; do
        echo -e "${FN_MESSAGE_PREFIX} ${cmd}" | tee -a "${logfile_fp}"
        eval "${cmd}" | tee -a "${logfile_fp}"
        ret_val="${PIPESTATUS[0]}"
        if [[ "${ret_val}" -ne 0 ]] ; then
            echo -e "${FN_ERROR_PREFIX} retval='${ret_val}', exiting ..." | tee -a "${logfile_fp}"
            exit 9
        fi
        echo | tee -a "${logfile_fp}" # newline
    done
    return 0
} # end function payload

function give_advice(){
    ## note local copies of readonly globals are forbidden in some bash versions :-(
#    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"  # just use the global
#    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"          # just use the global
    local ret_val=''
    local line # (line == '') -> newline

    for line in \
        "${FN_MESSAGE_PREFIX} If above offer looks acceptable, effect the upgrade with" \
        "    $ sudo ${APT_UPGRADE_HELPER} | tee -a ${logfile_fp}"                       \
        ''                                                                              \
        "${FN_MESSAGE_PREFIX} Later (e.g., after restart) re-query state with"          \
        "    $ ${STATE_QUERY_HELPER} | tee -a ${logfile_fp}"                            \
    ; do
        echo ${line} | tee -a "${logfile_fp}"
    done
    return 0
} # end function give_advice

### ----------------------------------------------------------------------
### main
### ----------------------------------------------------------------------

setup_logging
# can now write log
echo | tee -a "${logfile_fp}" # newline
test_helpers
# echo | tee -a "${logfile_fp}" # needs no separator unless it fails
payload
# no separator: `payload` writes its own
give_advice
echo | tee -a "${logfile_fp}" # newline

exit 0 # success!
