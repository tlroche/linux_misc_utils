#!/usr/bin/env bash

### Mount a (typically USB) drive from CLI (typically tty) with logging:
### takes log filepath on commandline, or uses own default filepath.
### Usecase/value-add for this script is, tty does not automount like desktop (e.g., tty7).
### Uses `udisksctl` (see https://github.com/storaged-project/udisks) as suggested @ https://askubuntu.com/a/865453/116758

### Currently very crude! TODO: update to current best-practice, e.g., rsync_push.sh

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

### logging-oriented vars part 1 of 2

## Messaging constants

declare -r CALLED_WITH="${*}" # "${@}" is array
declare -r THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare -r THIS_FN="$(basename "${THIS_FP}")"
declare -r THIS_DIR="$(readlink -f "$(dirname "${THIS_FP}")")"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare -r VERSION_N='0.1'
declare -r VERSION_STR="${THIS_FN} version=${VERSION_N}"
# TODO? use timestamp.sh or functions_library.sh
declare -r DATE_FORMAT='%Y%m%d_%H%M'

#echo -e "\n${DEBUG_PREFIX} called with '${CALLED_WITH}'" # debugging

### major vars

#declare -r USAGE="
declare -r BLOCK_DEVICE_ARG="${1}"
declare block_device_path=''       # set readonly later
declare mount_helper_command="udisksctl mount --block-device" # skeleton, WILL CHANGE BELOW
declare -r MOUNT_HELPER_EXEC_NAME="${mount_helper_command%% *}"

### minor vars

declare cmd=''
declare ret_val=''

### ----------------------------------------------------------------------
### main
### ----------------------------------------------------------------------

if   [[ -z "$(which "${MOUNT_HELPER_EXEC_NAME}")" ]] ; then
    >&2 echo "${ERROR_PREFIX} cannot execute '${MOUNT_HELPER_EXEC_NAME}': install first. Exiting ..."
    exit 3
elif [[ -z "${BLOCK_DEVICE_ARG}" ]] ; then
    >&2 echo "${ERROR_PREFIX} null BLOCK_DEVICE_ARG: use \`lsblk\` to determine name of block device (e.g. sdb1) to mount. Exiting ..."
    exit 4
## elif !exists(${BLOCK_DEVICE_ARG} || /dev/${BLOCK_DEVICE_ARG})
## `-b` == block device
elif [[ -b "${BLOCK_DEVICE_ARG}" ]] ; then
    block_device_path="${BLOCK_DEVICE_ARG}"
    readonly block_device_path
elif [[ -b "/dev/${BLOCK_DEVICE_ARG}" ]] ; then
    block_device_path="/dev/${BLOCK_DEVICE_ARG}"
    readonly block_device_path
else
    >&2 echo "${ERROR_PREFIX} cannot find block device to mount @ '${BLOCK_DEVICE_ARG}' or '/dev/${BLOCK_DEVICE_ARG}'"
    >&2 echo -e "\tUse \`lsblk\` to determine name of block device (e.g. 'sdb1') to mount."
    >&2 echo -e "\tExiting ..."
    exit 5
fi

mount_helper_command="${MOUNT_HELPER_EXEC_NAME} mount --block-device ${block_device_path} --no-user-interaction"
for cmd in \
    'date' \
    "${mount_helper_command}" \
; do
    echo "${MESSAGE_PREFIX} ${cmd}"
    eval "${cmd}"
    ret_val="${?}"
    if [[ "${ret_val}" -ne 0 ]] ; then
        >&2 echo "${ERROR_PREFIX} retval='${ret_val}', exiting ..."
        exit 6
    fi
done
echo # newline

exit 0
