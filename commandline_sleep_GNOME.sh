#!/usr/bin/env bash

### Sleep aka ('suspend to RAM') from CLI.
### Usecase/value-add for this script--on at least one of my GNOME boxen--is Fn-F4 does not work on tty1

### Currently very crude!
### Good news: following works on newer TP, from both tty1 and tty7 (terminal):
### + box sleeps on invocation
### + on hitting power button, box wakes/resumes to lock screen (normally, obeying set preference)
### Bad news: fails from older whitebox tty1 (its X is currently broken):
### + box sleeps on invocation
### - on hitting power button, box's LEDs indicate wake, but screen remains black and unresponsive to anything I tried except Alt-SysRq-[REISUB]

### TODO:
### * make work from tty1 on all my GNOME boxen!
### * parameterize for hibernation (see https://askubuntu.com/a/131022/116758)
### * handle syntax changes between GNOME/D-bus versions: see my comments below regarding 'org.freedesktop' keys/paths
### ** use `gdbus` after testing `which gdbus`
### * handle other window managers, e.g. KDE (see above plus https://askubuntu.com/q/1792/116758 )
### * use `systemd` if available
### * use `gnome-screensaver-command --lock` if available
### * update to current script best-practice, e.g., rsync_push.sh

### ----------------------------------------------------------------------
### constants
### ----------------------------------------------------------------------

declare -r CALLED_WITH="${*}" # "${@}" is array
declare -r THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare -r THIS_FN="$(basename "${THIS_FP}")"
declare -r THIS_DIR="$(readlink -f "$(dirname "${THIS_FP}")")"  # FQ path to caller's $(pwd)
declare -r MESSAGE_PREFIX="${THIS_FN}:"
declare -r DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
declare -r ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

declare checker_cmd=''
declare actor_cmd=''
declare o_f_key=''
declare -i o_f_key_hits=0

### ----------------------------------------------------------------------
### main
### ----------------------------------------------------------------------

for o_f_key in 'UPower' 'login1' ; do
    for checker_cmd in \
        "gdbus introspect --system --dest 'org.freedesktop.${o_f_key}' --object-path '/org/freedesktop/${o_f_key}' --recurse | fgrep -ie 'suspend'" \
        ; do
        o_f_key_hits="$(eval "${checker_cmd} | wc -l")"
        # TODO: test retval
#        echo "DEBUG: o_f_key='${o_f_key}' -> o_f_key_hits='${o_f_key_hits}'"
        if (( o_f_key_hits > 0 )) ; then
            actor_cmd="dbus-send --system --print-reply --dest='org.freedesktop.${o_f_key}' /org/freedesktop/${o_f_key} org.freedesktop.${o_f_key}.Manager.Suspend boolean:true"
            echo "${MESSAGE_PREFIX} ${actor_cmd}"
            eval "${actor_cmd}"
            ret_val="${?}"
            if [[ "${ret_val}" -ne 0 ]] ; then
                >&2 echo "${ERROR_PREFIX} retval='${ret_val}', exiting ..."
                exit 3
            fi
        else
            echo "${MESSAGE_PREFIX} org.freedesktop.${o_f_key} not found"
        fi
    done
done

exit 0
