#!/usr/bin/env bash

### Update NVIDIA video driver from LMDE using `sgfxi` (so I should use a less generic name :-)
### Main value-add for this script is logging, which is useful since `sgfxi` must run from tty.

### TODO:
### * allow taking logfile path via option.
### * check for `sgfxi`: if not found, throw error and offer to install.
### * before rest of run, query that user is running NVIDIA video driver (else rest of this script is useless if not dangerous).
### * before rest of run, query current NVIDIA driver version with `nvidia-settings --version`

### Copyright (C) 2017 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### https://bitbucket.org/tlroche/linux_misc_utils/src/HEAD/update_video_driver.sh

### ----------------------------------------------------------------------
### setup
### ----------------------------------------------------------------------

## for logging

THIS_FP="${0}"
THIS_DIR="$(readlink -f $(dirname ${THIS_FP}))" # FQ/absolute path
THIS_FN="$(basename ${THIS_FP})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
DATE_FORMAT='%Y%m%d_%H%M'
TIMESTAMP="$(date +${DATE_FORMAT})"
LOGFILE_FN="${THIS_FN%%\.sh}_log__${TIMESTAMP}.txt"
if [[ ( -z "${THIS_DIR}" ) || ( "${THIS_DIR}" == '/' ) ]] ; then
    LOGFILE_DIR='/tmp'
else
    LOGFILE_DIR="${THIS_DIR}"
fi
LOGFILE_FP="${LOGFILE_DIR}/${LOGFILE_FN}"
# echo -e "${DEBUG_PREFIX} LOGFILE_DIR='${LOGFILE_DIR}'"
# echo -e "${DEBUG_PREFIX} LOGFILE_FP='${LOGFILE_FP}'"

## misc

RETVAL=0 # test {return values, errorcodes}
SGFXI_OPTIONS='-j 0' # monochrome output

### ----------------------------------------------------------------------
### functions
### ----------------------------------------------------------------------

### TODO: move to standard-functions file
setup_logging(){
    local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
    local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
    local DEBUG_PREFIX="${MESSAGE_PREFIX} DEBUG:"
    local RETVAL=0

    if   [[ -z "${LOGFILE_FP}" ]] ; then
        echo "${ERROR_PREFIX}: LOGFILE_FP not defined, exiting..." 1>&2
        echo # newline
        exit 2
    elif [[ -r "${LOGFILE_FP}" ]] ; then
        echo "${ERROR_PREFIX}: logfile='${LOGFILE_FP}' exists: move or delete. Exiting..." 1>&2
        echo # newline
        exit 3
    else
        touch "${LOGFILE_FP}"
        RETVAL="$?"
        if [[ ( "${RETVAL}" -ne 0 ) || ( ! -w "${LOGFILE_FP}" ) ]] ; then
            echo "${ERROR_PREFIX}: cannot write logfile='${LOGFILE_FP}', exiting..." 1>&2
            echo # newline
            exit 4
        else
            echo "${MESSAGE_PREFIX}: logging to file='${LOGFILE_FP}'" | tee -a "${LOGFILE_FP}"
            return 0
        fi
    fi
} # end setup_logging()

### ----------------------------------------------------------------------
### payload
### ----------------------------------------------------------------------

# [[ -n "${SUDO_USER}" ]] copy/mod from sgfxi
if   [[ ( "$(id -u)" != '0' ) || ( -n "${SUDO_USER}" ) ]] ; then
    # TODO: all my bash scripts: if ERROR_PREFIX then write stderr (1>&2)
    echo "${ERROR_PREFIX}: must be run as root (not just 'sudo'), exiting..." 1>&2
    echo # newline
    exit 5
elif [[ -n "${DISPLAY}" ]] ; then # DISPLAY set by X: copy/mod from sgfxi
    # sgfxi requires tty
    echo "${ERROR_PREFIX}: must be run from tty (not X/desktop), exiting..." 1>&2
    echo # newline
    exit 6
fi

setup_logging
RETVAL="$?"
# ASSERT: error already written
if [[ "${RETVAL}" -ne 0 ]] ; then
    exit ${RETVAL}
fi
# else can now write log

echo "${MESSAGE_PREFIX}: about to run sgfxi @ $(date):" | tee -a "${LOGFILE_FP}"
echo -e "\t$(sgfxi ${SGFXI_OPTIONS} --version | fgrep -e 'sgfxi')" | tee -a "${LOGFILE_FP}"
#RETVAL="$?"
# $? only works for end of pipeline
RETVAL="${PIPESTATUS[0]}"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} could not run 'sgfxi --version', exiting..." 1>&2 | tee -a "${LOGFILE_FP}"
    exit 7
fi

# run it!
sgfxi ${SGFXI_OPTIONS} | tee -a "${LOGFILE_FP}"
# $? only works for end of pipeline
RETVAL="${PIPESTATUS[0]}"
if [[ "${RETVAL}" -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} 'sgfxi' exited with errorcode='${RETVAL}'" 1>&2 | tee -a "${LOGFILE_FP}"
    exit 8
fi

exit 0 # success!
